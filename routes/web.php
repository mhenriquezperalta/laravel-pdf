<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');

    // $pdf = PDF::loadHTML('<h1 style="color:red">Test</h1>');
    
    // con loadView se carga vista de laravel(blade)
    $pdf = PDF::loadView('example');

    // visualizar
    return $pdf->stream();
    
    // descargar
    // return $pdf->download();
});
