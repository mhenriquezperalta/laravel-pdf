<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Receta Farmacia</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
        <table style="width:100%">
            <tr>
              <th>Firstname</th>
              <th>Lastname</th>
            </tr>
            <tr>
              <td>Jill</td>
              <td>Smith</td>
            </tr>
            <tr>
              <td>Eve</td>
              <td>Jackson</td>
            </tr>
            <tr>
              <td>John</td>
              <td>Doe</td>
            </tr>
          </table>
          <br>
        <table style="width:100%">
            <tr>
              <th colspan="2">RECETA A PACIENTE</th>
              {{-- <th>Age</th> --}}
            </tr>
            <tr>
              <td>Jill</td>
              <td>Smith</td>
              {{-- <td>43</td> --}}
            </tr>
            <tr>
              <td>Eve</td>
              <td>Jackson</td>
              {{-- <td>57</td> --}}
            </tr>
            <tr>
                <td>Eve</td>
                <td>Jackson</td>
                {{-- <td>57</td> --}}
              </tr>
              <tr>
                <td>Eve</td>
                <td>Jackson</td>
                {{-- <td>57</td> --}}
              </tr>
              <tr>
                <td>Eve</td>
                <td>Jackson</td>
                {{-- <td>57</td> --}}
              </tr>
        </table>
        <br>
        <table style="width:100%">
            <thead>
                <tr>
                  <th style="width:20%">CODIGO</th>
                  <th>CANT.</th>
                  <th>NOMBRE MEDICAMENTO</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>AA2104</td>
                  <td>3</td>
                  <td>50</td>
                </tr>
                <tr>
                  <td>AJ7108</td>
                  <td>2</td>
                  <td>94</td>
                </tr>
                <tr>
                  <td>AJ</td>
                  <td>1</td>
                  <td>80</td>
                </tr>
            </tbody>
        </table>
        {{-- <div class="container">
            <div class="alert alert-danger">Alerta</div>
        </div> --}}
    </body>
</html>
